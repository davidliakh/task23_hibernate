package epam.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "invertory", schema = "pharmacy", catalog = "")
@IdClass(InvertoryEntityPK.class)
public class InvertoryEntity {
    private int idinvertory;
    private int drugId;
    private Integer quantity;
    private BigDecimal priceBefore;
    private BigDecimal storePrice;

    @Id
    @Column(name = "idinvertory")
    public int getIdinvertory() {
        return idinvertory;
    }

    public void setIdinvertory(int idinvertory) {
        this.idinvertory = idinvertory;
    }

    @Id
    @Column(name = "drug_id")
    public int getDrugId() {
        return drugId;
    }

    public void setDrugId(int drugId) {
        this.drugId = drugId;
    }

    @Basic
    @Column(name = "quantity")
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "price_before")
    public BigDecimal getPriceBefore() {
        return priceBefore;
    }

    public void setPriceBefore(BigDecimal priceBefore) {
        this.priceBefore = priceBefore;
    }

    @Basic
    @Column(name = "store_price")
    public BigDecimal getStorePrice() {
        return storePrice;
    }

    public void setStorePrice(BigDecimal storePrice) {
        this.storePrice = storePrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvertoryEntity that = (InvertoryEntity) o;

        if (idinvertory != that.idinvertory) return false;
        if (drugId != that.drugId) return false;
        if (quantity != null ? !quantity.equals(that.quantity) : that.quantity != null) return false;
        if (priceBefore != null ? !priceBefore.equals(that.priceBefore) : that.priceBefore != null) return false;
        if (storePrice != null ? !storePrice.equals(that.storePrice) : that.storePrice != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idinvertory;
        result = 31 * result + drugId;
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        result = 31 * result + (priceBefore != null ? priceBefore.hashCode() : 0);
        result = 31 * result + (storePrice != null ? storePrice.hashCode() : 0);
        return result;
    }
}
