package epam.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "payment", schema = "pharmacy", catalog = "")
public class PaymentEntity {
    private int idpayment;
    private Object paymentType;
    private Date date;
    private String bank;
    private String numberOfCard;

    @Id
    @Column(name = "idpayment")
    public int getIdpayment() {
        return idpayment;
    }

    public void setIdpayment(int idpayment) {
        this.idpayment = idpayment;
    }

    @Basic
    @Column(name = "payment_type")
    public Object getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Object paymentType) {
        this.paymentType = paymentType;
    }

    @Basic
    @Column(name = "date")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "bank")
    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    @Basic
    @Column(name = "number_of_card")
    public String getNumberOfCard() {
        return numberOfCard;
    }

    public void setNumberOfCard(String numberOfCard) {
        this.numberOfCard = numberOfCard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentEntity that = (PaymentEntity) o;

        if (idpayment != that.idpayment) return false;
        if (paymentType != null ? !paymentType.equals(that.paymentType) : that.paymentType != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (bank != null ? !bank.equals(that.bank) : that.bank != null) return false;
        if (numberOfCard != null ? !numberOfCard.equals(that.numberOfCard) : that.numberOfCard != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idpayment;
        result = 31 * result + (paymentType != null ? paymentType.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (bank != null ? bank.hashCode() : 0);
        result = 31 * result + (numberOfCard != null ? numberOfCard.hashCode() : 0);
        return result;
    }
}
