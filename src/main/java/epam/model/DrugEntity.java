package epam.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "drug", schema = "pharmacy", catalog = "")
public class DrugEntity {
    private String name;
    private BigDecimal price;
    private String components;
    private String producer;
    private String typeOfPackage;
    private String typeOfUse;
    private byte withRecipe;
    private Object typeOfUser;
    private String recipeNameOfDrug;

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic
    @Column(name = "components")
    public String getComponents() {
        return components;
    }

    public void setComponents(String components) {
        this.components = components;
    }

    @Basic
    @Column(name = "producer")
    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    @Basic
    @Column(name = "type_of_package")
    public String getTypeOfPackage() {
        return typeOfPackage;
    }

    public void setTypeOfPackage(String typeOfPackage) {
        this.typeOfPackage = typeOfPackage;
    }

    @Basic
    @Column(name = "type_of_use")
    public String getTypeOfUse() {
        return typeOfUse;
    }

    public void setTypeOfUse(String typeOfUse) {
        this.typeOfUse = typeOfUse;
    }

    @Basic
    @Column(name = "with_recipe")
    public byte getWithRecipe() {
        return withRecipe;
    }

    public void setWithRecipe(byte withRecipe) {
        this.withRecipe = withRecipe;
    }

    @Basic
    @Column(name = "type_of_user")
    public Object getTypeOfUser() {
        return typeOfUser;
    }

    public void setTypeOfUser(Object typeOfUser) {
        this.typeOfUser = typeOfUser;
    }

    @Basic
    @Column(name = "recipe_name_of_drug")
    public String getRecipeNameOfDrug() {
        return recipeNameOfDrug;
    }

    public void setRecipeNameOfDrug(String recipeNameOfDrug) {
        this.recipeNameOfDrug = recipeNameOfDrug;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DrugEntity that = (DrugEntity) o;

        if (withRecipe != that.withRecipe) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (components != null ? !components.equals(that.components) : that.components != null) return false;
        if (producer != null ? !producer.equals(that.producer) : that.producer != null) return false;
        if (typeOfPackage != null ? !typeOfPackage.equals(that.typeOfPackage) : that.typeOfPackage != null)
            return false;
        if (typeOfUse != null ? !typeOfUse.equals(that.typeOfUse) : that.typeOfUse != null) return false;
        if (typeOfUser != null ? !typeOfUser.equals(that.typeOfUser) : that.typeOfUser != null) return false;
        if (recipeNameOfDrug != null ? !recipeNameOfDrug.equals(that.recipeNameOfDrug) : that.recipeNameOfDrug != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (components != null ? components.hashCode() : 0);
        result = 31 * result + (producer != null ? producer.hashCode() : 0);
        result = 31 * result + (typeOfPackage != null ? typeOfPackage.hashCode() : 0);
        result = 31 * result + (typeOfUse != null ? typeOfUse.hashCode() : 0);
        result = 31 * result + (int) withRecipe;
        result = 31 * result + (typeOfUser != null ? typeOfUser.hashCode() : 0);
        result = 31 * result + (recipeNameOfDrug != null ? recipeNameOfDrug.hashCode() : 0);
        return result;
    }
}
