package epam.model;

import javax.persistence.*;

@Entity
@Table(name = "order_detail", schema = "pharmacy", catalog = "")
@IdClass(OrderDetailEntityPK.class)
public class OrderDetailEntity {
    private int id;
    private int drugId;
    private Integer quantityOrdered;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Id
    @Column(name = "drug_id")
    public int getDrugId() {
        return drugId;
    }

    public void setDrugId(int drugId) {
        this.drugId = drugId;
    }

    @Basic
    @Column(name = "quantity_ordered")
    public Integer getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(Integer quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderDetailEntity that = (OrderDetailEntity) o;

        if (id != that.id) return false;
        if (drugId != that.drugId) return false;
        if (quantityOrdered != null ? !quantityOrdered.equals(that.quantityOrdered) : that.quantityOrdered != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + drugId;
        result = 31 * result + (quantityOrdered != null ? quantityOrdered.hashCode() : 0);
        return result;
    }
}
