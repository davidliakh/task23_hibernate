package epam.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class InvertoryEntityPK implements Serializable {
    private int idinvertory;
    private int drugId;

    @Column(name = "idinvertory")
    @Id
    public int getIdinvertory() {
        return idinvertory;
    }

    public void setIdinvertory(int idinvertory) {
        this.idinvertory = idinvertory;
    }

    @Column(name = "drug_id")
    @Id
    public int getDrugId() {
        return drugId;
    }

    public void setDrugId(int drugId) {
        this.drugId = drugId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvertoryEntityPK that = (InvertoryEntityPK) o;

        if (idinvertory != that.idinvertory) return false;
        if (drugId != that.drugId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idinvertory;
        result = 31 * result + drugId;
        return result;
    }
}
