package epam.model;

import javax.persistence.*;

@Entity
@Table(name = "recipe", schema = "pharmacy", catalog = "")
public class RecipeEntity {
    private String nameOfDrug;
    private String nameOfDoctor;
    private String userName;
    private byte signFromDoctor;
    private int drugId1;

    @Id
    @Column(name = "name_of_drug")
    public String getNameOfDrug() {
        return nameOfDrug;
    }

    public void setNameOfDrug(String nameOfDrug) {
        this.nameOfDrug = nameOfDrug;
    }

    @Basic
    @Column(name = "name_of_doctor")
    public String getNameOfDoctor() {
        return nameOfDoctor;
    }

    public void setNameOfDoctor(String nameOfDoctor) {
        this.nameOfDoctor = nameOfDoctor;
    }

    @Basic
    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "sign_from_doctor")
    public byte getSignFromDoctor() {
        return signFromDoctor;
    }

    public void setSignFromDoctor(byte signFromDoctor) {
        this.signFromDoctor = signFromDoctor;
    }

    @Basic
    @Column(name = "drug_id1")
    public int getDrugId1() {
        return drugId1;
    }

    public void setDrugId1(int drugId1) {
        this.drugId1 = drugId1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecipeEntity that = (RecipeEntity) o;

        if (signFromDoctor != that.signFromDoctor) return false;
        if (drugId1 != that.drugId1) return false;
        if (nameOfDrug != null ? !nameOfDrug.equals(that.nameOfDrug) : that.nameOfDrug != null) return false;
        if (nameOfDoctor != null ? !nameOfDoctor.equals(that.nameOfDoctor) : that.nameOfDoctor != null) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nameOfDrug != null ? nameOfDrug.hashCode() : 0;
        result = 31 * result + (nameOfDoctor != null ? nameOfDoctor.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (int) signFromDoctor;
        result = 31 * result + drugId1;
        return result;
    }
}
